FROM registry.access.redhat.com/openjdk/openjdk-11-rhel7

MAINTAINER bdo-pay-devs@novare.com.hk

USER root

VOLUME /tmp

ARG JAR_FILE

COPY ${JAR_FILE} app.jar

EXPOSE 8083

HEALTHCHECK CMD curl --fail http://localhost:8083/bdo-pay/sapi/v1/transactions/actuator/health || exit 1

CMD ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "./app.jar"]
