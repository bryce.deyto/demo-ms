# Bdo Pay Demo Ms

Scaffolding for Spring Microservices

### Tech
* [Spring Boot](https://spring.io/projects/spring-boot/)
* [Hibernate](https://hibernate.org/)
* [Feign](https://spring.io/projects/spring-cloud-openfeign)
* [Lombok](https://projectlombok.org/)
* [Liquibase](http://www.liquibase.org/)
* [Checkstyle](http://checkstyle.sourceforge.net/)
* [Spotbugs](https://spotbugs.github.io/)
* [PMD](https://pmd.github.io/)
* [ArchUnit](https://www.archunit.org/)
* [Hamcrest](http://hamcrest.org/JavaHamcrest/)