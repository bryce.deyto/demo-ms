#!/bin/bash

echo "Pipeline: $CI_PIPELINE_IID" >> buildinfo
echo "BuildJob: $CI_JOB_ID" >> buildinfo
echo "Branch: $CI_COMMIT_REF_NAME" >> buildinfo
echo "Commit Hash: $CI_COMMIT_SHA" >> buildinfo
echo "Commit Message: $CI_COMMIT_MESSAGE" >> buildinfo
