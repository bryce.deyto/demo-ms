#!/bin/bash

echo "=== Initialize container variables..."
chmod 700 $8/$4/export_variables.sh && bash $8/$4/export_variables.sh $1 $8/$4

ENV=$1
CI_PROJECT_NAME=$2
CI_BUILD_TOKEN=$3
IMAGE_TAG=$4
REGISTRY_HOST=$5
PORT=$6
DB_HOST=$7
SERVER_PATH=$8
ENV_FILE_PATH=$8/$4

echo "=== Setting variables..."
echo "CI_PROJECT_NAME: $CI_PROJECT_NAME"
echo "CI_BUILD_TOKEN: $CI_BUILD_TOKEN"
echo "IMAGE_TAG: $IMAGE_TAG"
echo "ENV: $ENV"
echo "REGISTRY_HOST: $REGISTRY_HOST"
echo "PORT: $PORT"
echo "DB_HOST: $DB_HOST"
echo "SERVER_PATH: $SERVER_PATH"
echo "ENV_FILE_PATH: $ENV_FILE_PATH"
echo "=== Done setting variables..."

echo "=== Starting pre-deployment clean-up..."
docker stop $(docker ps -a | grep "$CI_PROJECT_NAME" | awk '{print $1;}')
docker rm $(docker ps -a | grep "$CI_PROJECT_NAME" | awk '{print $1;}')
docker rmi $(docker images | grep "$CI_PROJECT_NAME" | awk '{print $3;}')
echo "=== Done with pre-deployment clean-up..."

echo "=== Starting deployment..."
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
docker pull "$REGISTRY_HOST/$CI_PROJECT_NAME:$IMAGE_TAG"
docker run -p $PORT:$PORT --name=$CI_PROJECT_NAME --restart=on-failure --env-file $ENV_FILE_PATH/variables.env --add-host=database:$DB_HOST -d "$REGISTRY_HOST/$CI_PROJECT_NAME:$IMAGE_TAG"
echo "=== Done with deployment..."

echo "=== Starting post-deployment clean-up..."
rm -rf $ENV_FILE_PATH
echo "=== Done with post-deployment clean-up..."
