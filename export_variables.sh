#!/bin/bash
ENV=$1
PATH_TO_VARS=$2

echo Remove ENV list first...
rm -f env.list
rm -f env-vars.list

printenv > env.list
source $PATH_TO_VARS/variables.sh $ENV
printenv > env-vars.list
grep -v -F -x -f env.list env-vars.list > $PATH_TO_VARS/variables.env
rm -f env.list
rm -f env-vars.list

echo Init done...
