#!/bin/bash

mvn install:install-file \
  -Dfile="jars/bdo-pay-commons-starter.jar" \
  -DgroupId=ph.com.bdo.pay \
  -DartifactId=bdo-pay-commons-starter \
  -Dversion=1.0.0 \
  -Dpackaging=jar \
  -DgeneratePom=true

mvn install:install-file \
 -Dfile="jars/bdo-pay-error-codes.jar" \
 -DgroupId=com.bdo.error \
 -DartifactId=bdo-pay-error-codes \
 -Dversion=1.0.0 \
 -Dpackaging=jar \
 -DgeneratePom=true

