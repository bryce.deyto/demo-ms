package ph.com.bdo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import ph.com.bdo.demo.config.feign.FeignWithoutFallbackConfig;

@EnableJpaAuditing
@SpringBootApplication
@EnableFeignClients
@ComponentScan(
    excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,
            value = FeignWithoutFallbackConfig.class)
    })
public class MainApp {
    public static void main(final String[] args) {
        SpringApplication.run(MainApp.class, args);
    }
}
