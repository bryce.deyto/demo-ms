package ph.com.bdo.demo.api.sample.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ph.com.bdo.demo.api.sample.dto.SampleRequest;
import ph.com.bdo.demo.api.sample.dto.SampleResponse;
import ph.com.bdo.demo.api.sample.model.Sample;
import ph.com.bdo.demo.api.sample.service.SampleService;
import ph.com.bdo.pay.commons.starter.util.future.FutureUtil;
import ph.com.bdo.pay.commons.starter.util.pagination.dto.PaginationResponse;
import ph.com.bdo.pay.commons.starter.util.restutil.dto.ResponseType;
import ph.com.bdo.pay.commons.starter.util.restutil.enums.ResponseTypeConstantUtils;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@RestController
@RequestMapping("api/sample")
@RequiredArgsConstructor
public class SampleController {

    private final SampleService sampleService;

    @Qualifier("applicationTaskExecutor")
    @Autowired
    private Executor executor;

    /**
     * Get All student event with pagination
     * ResponseType annotation is important for returning the body
     *
     * @return ClassTypeList
     */
    @GetMapping
    @SneakyThrows
    @ResponseType(type = ResponseTypeConstantUtils.PAGINATION)
    @ApiOperation(value = "Retrieve all Students")
    public ResponseEntity<PaginationResponse> retrieveAllWithPagination(
            @RequestParam(value = "limit", defaultValue = "${settings.pagination.limit}") final Integer limit,
            @RequestParam(value = "offset", defaultValue = "${settings.pagination.offset}") final Integer offset,
            final HttpServletRequest request
    ) {
        final CompletableFuture<PaginationResponse> future = FutureUtil.toCompletable(
                CompletableFuture.completedFuture(sampleService.getAll(limit, offset, request)),
                executor
        );
        future.complete(sampleService.getAll(limit, offset, request));

        return new ResponseEntity<>(
                future.get(),
                HttpStatus.OK
        );
    }

    /**
     * Add new Sample Object and save to DB
     *
     * @return ResponseEntity<SampleResponse>
     */
    @PostMapping
    @SneakyThrows
    public ResponseEntity<SampleResponse> addSample(@RequestBody final SampleRequest request) {
        final CompletableFuture<Sample> future = FutureUtil.toCompletable(
                CompletableFuture.completedFuture(sampleService.save(request)),
                executor
        );

        return new ResponseEntity<>(new SampleResponse(future.get()), HttpStatus.OK);
    }
}
