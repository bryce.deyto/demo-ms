package ph.com.bdo.demo.api.sample.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import ph.com.bdo.pay.commons.starter.util.json.JsonObject;
import ph.com.bdo.pay.commons.starter.util.validator.ParameterMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class SampleRequest extends JsonObject {
    private String sampleString;
    private String sampleInt;
    private String sampleDouble;
    private String sampleLong;
    private String sampleBoolean;

    /**
     * To Parameter Map
     * @return ParameterMap of ApplicationFieldParametersRequestJson
     */
    public ParameterMap toParameterMap() {
        return new ObjectMapper().convertValue(this, ParameterMap.class);
    }

    /**
     * To Parameter Map for Updating
     * @return ParameterMap of ApplicationFieldParametersRequestJson
     */
    public ParameterMap toParameterMap(final SampleRequest oldSampleRequest) {
        return toParameterMap(this, oldSampleRequest);
    }
}
