package ph.com.bdo.demo.api.sample.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;
import ph.com.bdo.demo.api.sample.model.Sample;
import java.util.Date;

@Data
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public final class SampleResponse {
    private String sampleString;

    private int sampleInt;

    private double sampleDouble;

    private long sampleLong;

    private boolean sampleBoolean;

    private Date createdDate;

    public SampleResponse(final Sample sample) {
        setSampleString(sample.getSampleString());
        setSampleInt(sample.getSampleInt());
        setSampleDouble(sample.getSampleDouble());
        setSampleLong(sample.getSampleLong());
        setSampleBoolean(sample.isSampleBoolean());
        setCreatedDate(sample.getCreatedDate());
    }
}
