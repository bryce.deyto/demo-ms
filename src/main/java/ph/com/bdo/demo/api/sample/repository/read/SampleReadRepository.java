package ph.com.bdo.demo.api.sample.repository.read;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ph.com.bdo.demo.api.sample.model.Sample;
import java.util.Optional;

@Repository
public interface SampleReadRepository extends CrudRepository<Sample, Long> {
    Optional<Sample> findByIdEquals(String id);

    Page<Sample> findAll(Pageable pageable);
}
