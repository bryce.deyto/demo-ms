package ph.com.bdo.demo.api.sample.repository.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ph.com.bdo.demo.api.sample.model.Sample;
import java.util.Optional;

public interface SampleRepoService {
    Optional<Sample> findByIdEquals(String id);

    Page<Sample> findAll(Pageable pageable);

    Sample save(Sample sample);
}
