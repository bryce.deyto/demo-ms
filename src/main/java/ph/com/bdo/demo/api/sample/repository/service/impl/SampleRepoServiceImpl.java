package ph.com.bdo.demo.api.sample.repository.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ph.com.bdo.demo.api.sample.model.Sample;
import ph.com.bdo.demo.api.sample.repository.read.SampleReadRepository;
import ph.com.bdo.demo.api.sample.repository.service.SampleRepoService;
import ph.com.bdo.demo.api.sample.repository.write.SampleWriteRepository;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public final class SampleRepoServiceImpl implements SampleRepoService {

    private final SampleReadRepository readRepo;
    private final SampleWriteRepository writeRepo;

    @Override
    public Optional<Sample> findByIdEquals(final String id) {
        return readRepo.findByIdEquals(id);
    }

    @Override
    public Page<Sample> findAll(final Pageable pageable) {
        return readRepo.findAll(pageable);
    }

    @Override
    public Sample save(final Sample sample) {
        return writeRepo.save(sample);
    }
}
