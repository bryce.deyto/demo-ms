package ph.com.bdo.demo.api.sample.repository.write;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ph.com.bdo.demo.api.sample.model.Sample;

@Repository
public interface SampleWriteRepository extends CrudRepository<Sample, Long> {
}
