package ph.com.bdo.demo.api.sample.service;

import ph.com.bdo.demo.api.sample.dto.SampleRequest;
import ph.com.bdo.demo.api.sample.model.Sample;
import ph.com.bdo.pay.commons.starter.util.pagination.dto.PaginationResponse;

import javax.servlet.http.HttpServletRequest;

public interface SampleService {
    Sample save(SampleRequest request);

    PaginationResponse getAll(
            Integer limit,
            Integer offset,
            HttpServletRequest request
    );
}
