package ph.com.bdo.demo.api.sample.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ph.com.bdo.demo.api.sample.dto.SampleRequest;
import com.bdo.pay.errors.exceptions.ms.demo.SampleGenericException;
import ph.com.bdo.demo.api.sample.model.Sample;
import ph.com.bdo.demo.api.sample.repository.service.SampleRepoService;
import ph.com.bdo.demo.api.sample.service.SampleService;
import ph.com.bdo.demo.api.sample.validator.SampleValidator;
import ph.com.bdo.pay.commons.starter.util.validator.ParameterMap;
import ph.com.bdo.pay.commons.starter.util.pagination.PaginationUtils;
import ph.com.bdo.pay.commons.starter.util.pagination.dto.Meta;
import ph.com.bdo.pay.commons.starter.util.pagination.dto.OffsetBasedPageRequest;
import ph.com.bdo.pay.commons.starter.util.pagination.dto.PaginationResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public final class SampleServiceImpl implements SampleService {
    private final SampleRepoService repoService;
    private final SampleValidator validator;

    @Override
    public Sample save(final SampleRequest request) {
        Sample sample = new Sample();
        final ParameterMap body = request.toParameterMap();
        final Optional<Sample> sampleOptional = validator.validate(body);

        if (sampleOptional.isPresent()) {
            sample = repoService.save(sampleOptional.get());
        }

        return sample;
    }

    @Override
    public PaginationResponse getAll(final Integer limit, final Integer offset, final HttpServletRequest request) {
        final Pageable pageable = new OffsetBasedPageRequest(offset, limit);
        final Page<Sample> samplePage = repoService.findAll(pageable);
        final List<Sample> sampleList = samplePage.getContent();

        if (sampleList.isEmpty()) {
            throw new SampleGenericException();
        }

        final PaginationResponse paginationResponse = new PaginationResponse();
        paginationResponse.setData(sampleList);
        paginationResponse.setMeta(
                new Meta((int) samplePage.getTotalElements(),
                        samplePage.getTotalPages(),
                        (int) samplePage.getTotalElements())
        );
        paginationResponse.setLinks(PaginationUtils.getLinks(request, offset, limit, samplePage));

        return paginationResponse;
    }
}
