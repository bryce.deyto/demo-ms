package ph.com.bdo.demo.api.sample.validator;

import org.springframework.stereotype.Component;
import com.bdo.pay.errors.exceptions.ms.demo.SampleGenericException;
import com.bdo.pay.errors.exceptions.ms.demo.SampleInvalidIntValueException;
import ph.com.bdo.demo.api.sample.model.Sample;
import ph.com.bdo.pay.commons.starter.util.validator.ParameterMap;
import ph.com.bdo.pay.commons.starter.util.validator.Validator;
import java.util.Optional;

@Component
public final class SampleValidator extends Validator<Optional<Sample>> {

    @Override
    protected Optional<Sample> doValidate(final ParameterMap body) {
        // 1 Instantiate object with Optional.ofNullable() and get from body (used in update) or return new Object
        final Sample sample = Optional.ofNullable(
                (Sample) body.get("sample")
        ).orElse(Sample.builder().build());

        // 2 Create Optional Object
        final Optional<Sample> sampleOptional;

        // 3 Call overridden validateFields method
        validateFields(body);

        // Step 4 inside validateFields

        // 5 Create method build(T object, ParameterMap body)

        // Step 6 inside build

        // 7 Set value of Optional Object to build
        sampleOptional = Optional.of(build(sample, body));

        // 8 Return Optional Object
        return sampleOptional;
    }

    @Override
    protected void validateFields(final ParameterMap body) {
        // 4 Create Field Validations
        final int maxLength = 10;
        final double maxValue = 50;

        // sample_string validations - required, minimum length : 5
        body.validate("SampleString")
                .setException(new SampleGenericException())
                .required()
                .maxLength(maxLength);

        // sample_int validations - required, int data type, minimum value : 0
        body.validate("SampleInt")
                .setException(new SampleInvalidIntValueException())
                .required()
                .isInt()
                .minValue(5);

        // sample_double validations - required, double data type
        body.validate("SampleDouble")
                .setException(new SampleGenericException())
                .isDouble();

        // sample_long validations - required, long data type
        body.validate("SampleLong")
                .setException(new SampleGenericException())
                .isLong();

        // sample_boolean validations - required, boolean data type
        body.validate("SampleBoolean")
                .setException(new SampleGenericException())
                .isBoolean();
    }

    protected Sample build(final Sample sample, final ParameterMap body) {
        // 6 Set values to T
        sample.setSampleString(
                body.validate("SampleString").getValue()
        );

        sample.setSampleInt(
                Integer.parseInt(body.validate("SampleInt").getValue())
        );

        sample.setSampleDouble(
                Double.parseDouble(body.validate("SampleDouble").getValue())
        );

        sample.setSampleLong(
                Long.parseLong(body.validate("SampleLong").getValue())
        );

        sample.setSampleBoolean(
                Boolean.parseBoolean(body.validate("SampleBoolean").getValue())
        );

        return sample;
    }
}
