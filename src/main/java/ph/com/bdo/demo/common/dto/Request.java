package ph.com.bdo.demo.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
@Data
@AllArgsConstructor
public final class Request {

    private RequestHeaders requestHeaders;

    private RequestParams requestParams;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object requestBody;
}
