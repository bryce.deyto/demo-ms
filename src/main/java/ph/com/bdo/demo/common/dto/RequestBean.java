package ph.com.bdo.demo.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RequestBean {
    private Map<String, String> requestHeaders;
    private Map<String, String> requestParams;
    private Object requestBody;
    private String referenceNumber;
}
