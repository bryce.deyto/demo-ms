package ph.com.bdo.demo.common.exception;

import com.bdo.pay.errors.commons.ErrorHandlerResponse;
import com.bdo.pay.errors.exceptions.ErrorCodeException;
import com.bdo.pay.errors.exceptions.ms.demo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.HandlerMethod;
import ph.com.bdo.demo.api.sample.controller.SampleController;
import ph.com.bdo.pay.commons.starter.util.restutil.GlobalResponseHandler;

import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/05/2021
 */
@ControllerAdvice(basePackageClasses = {SampleController.class})
@Slf4j
public final class GlobalExceptionHandler extends GlobalResponseHandler {
    private final HttpHeaders headers = new HttpHeaders();

    @ExceptionHandler({
        DemoStudentsNotFoundException.class,
        DemoSchoolsNotFoundException.class,
        SampleGenericException.class,
        SampleInvalidIntValueException.class,
        CompletionException.class
    })
    public ResponseEntity<ErrorHandlerResponse> handleBadRequestError(
        final ErrorCodeException ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        final ErrorHandlerResponse errorResponse = new ErrorHandlerResponse(
            ex,
            ex.getMessage()
        );

        log.error("[BadRequestException] Handler: {} ErrorCode: {}, Message: {}",
            handlerMethod.getMethod().getDeclaringClass(),
            errorResponse.getErrorCode(), errorResponse.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @SuppressWarnings({
        "checkstyle:CyclomaticComplexity",
        "checkstyle:ExecutableStatementCount"
    })
    @ExceptionHandler({
        HttpClientErrorException.class,
        HttpServerErrorException.class
    })
    public ResponseEntity<Object> handleHttpException(
        final HttpStatusCodeException ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        final ConcurrentHashMap<String, String> responseMap = new ConcurrentHashMap<>();
        // final String exceptionMessage = ex.getResponseBodyAsString();
        final HttpStatus status = ex.getStatusCode();

        log.info("[HttpErrorException] Handler: {} Code: {} Message: {}",
            handlerMethod.getMethod().getDeclaringClass(),
            status, ex.getResponseBodyAsString());
        return new ResponseEntity<>(
            !responseMap.containsKey("ErrorCode") ? ex.getResponseBodyAsString() : responseMap, status);
    }

    @ExceptionHandler(AccountIdInsufficientBalanceException.class)
    public ResponseEntity<ErrorHandlerResponse> handleForbiddenRequest(
        final ErrorCodeException ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        final ErrorHandlerResponse errorResponse = new ErrorHandlerResponse(
            ex,
            ex.getMessage()
        );

        log.error("[ForbiddenRequestException] Handler: {} ErrorCode: {}, Message: {}",
            handlerMethod.getMethod().getDeclaringClass(),
            errorResponse.getErrorCode(), errorResponse.getMessage());
        return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    public ResponseEntity<ErrorHandlerResponse> handleInternalServerExceptions(
        final ErrorCodeException ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.TEXT_PLAIN);
        log.error("[InternalServerException] Handler: {} Message: {}",
            handlerMethod.getMethod().getDeclaringClass(), ex.getMessage());
        final ErrorHandlerResponse handlerResponse = new ErrorHandlerResponse(
            ex,
            ex.getMessage()
        );
        return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleRuntimeExceptions(
        final Exception ex,
        final HandlerMethod handlerMethod
    ) {
        headers.setContentType(MediaType.TEXT_PLAIN);
        log.error("[RunTimeException] Handler: {} Message: {}",
            handlerMethod.getMethod().getDeclaringClass(), ex.getMessage());
        ex.printStackTrace();
        return new ResponseEntity<>(ex.getSuppressed(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(
        final MissingPathVariableException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        log.error("[MissingPathVariable] Message: {} {}", status, ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), status);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
        final MissingServletRequestParameterException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        log.error("[MissingServletRequestParameter] Message: {} {}", status, ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), status);
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(
        final ServletRequestBindingException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        final InternalServerErrorException internalServerError = new InternalServerErrorException();

        final ErrorHandlerResponse handlerResponse = new ErrorHandlerResponse(
            internalServerError,
            internalServerError.getMessage()
        );

        return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
        final HttpMessageNotReadableException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        final InternalServerErrorException internalServerError = new InternalServerErrorException();

        final ErrorHandlerResponse handlerResponse = new ErrorHandlerResponse(
            internalServerError,
            internalServerError.getMessage()
        );

        return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(
        final TypeMismatchException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        final InternalServerErrorException internalServerError = new InternalServerErrorException();

        final ErrorHandlerResponse handlerResponse = new ErrorHandlerResponse(
            internalServerError,
            internalServerError.getMessage()
        );

        return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        final MethodArgumentNotValidException ex,
        final HttpHeaders headers,
        final HttpStatus status,
        final WebRequest request
    ) {
        final InternalServerErrorException internalServerError = new InternalServerErrorException();

        final ErrorHandlerResponse handlerResponse = new ErrorHandlerResponse(
            internalServerError,
            internalServerError.getMessage()
        );

        return new ResponseEntity<>(handlerResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
