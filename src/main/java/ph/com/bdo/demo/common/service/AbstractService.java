package ph.com.bdo.demo.common.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
public abstract class AbstractService<T> {

    protected final List<T> convertToList(final Iterable<T> iterable) {
        final List<T> list = new ArrayList<>();
        iterable.iterator().forEachRemaining(list::add);

        return list;
    }
}
