package ph.com.bdo.demo.config.actuator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.jdbc.DataSourceHealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Add DB Health Check.
 * @author john.santiago@novare.com.hk
 */
@Configuration
public class DataSourceActuatorConfig {

    @Bean
    public HealthIndicator writeDbHealthIndicator(
        @Autowired final DataSource dataSource,
        @Value("${spring.datasource.url}") final String url
    ) {
        return () -> {
            final HealthIndicator indicator = new DataSourceHealthIndicator(dataSource);

            return Health.status(indicator.health().getStatus())
                .withDetail("url", url)
                .withDetail("dependency", "required")
                .build();
        };
    }

    @Bean
    public HealthIndicator readDbHealthIndicator(
        @Autowired final DataSource dataSource,
        @Value("${spring.datasource.read.url}") final String url
    ) {
        return () -> {
            final HealthIndicator indicator = new DataSourceHealthIndicator(dataSource);

            return Health.status(indicator.health().getStatus())
                .withDetail("url", url)
                .withDetail("dependency", "required")
                .build();
        };
    }
}
