package ph.com.bdo.demo.config.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
@EnableAsync
@Configuration
public class AsyncTaskConfiguration implements AsyncConfigurer {
    private final Integer poolSize;

    @Autowired
    public AsyncTaskConfiguration(@Value("${threads.default.size:10}") final Integer poolSize) {
        this.poolSize = poolSize;
    }

    @Override
    public final Executor getAsyncExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setMaxPoolSize(poolSize);
        executor.setQueueCapacity(poolSize);
        executor.setThreadNamePrefix("Student-Client-");
        executor.initialize();

        return executor;
    }
}
