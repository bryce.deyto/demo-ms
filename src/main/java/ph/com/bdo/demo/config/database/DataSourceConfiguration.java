package ph.com.bdo.demo.config.database;

import org.springframework.cloud.aws.jdbc.config.annotation.EnableRdsInstance;
import org.springframework.cloud.aws.jdbc.config.annotation.RdsInstanceConfigurer;
import org.springframework.cloud.aws.jdbc.datasource.TomcatJdbcDataSourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@Profile("bdo")
@EnableRdsInstance(dbInstanceIdentifier = "bdo", password = "P@ssw0rd1", readReplicaSupport = true)
public class DataSourceConfiguration {
    private static final Integer INITIALIZE_SIZE = 10;

    @Bean
    public RdsInstanceConfigurer instanceConfigurer() {
        return () -> {
            final TomcatJdbcDataSourceFactory dataSourceFactory = new TomcatJdbcDataSourceFactory();
            dataSourceFactory.setInitialSize(INITIALIZE_SIZE);
            dataSourceFactory.setValidationQuery("SELECT 1");
            return dataSourceFactory;
        };
    }
}
