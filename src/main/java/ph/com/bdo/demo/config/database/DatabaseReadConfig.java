package ph.com.bdo.demo.config.database;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
@EnableJpaRepositories(
    basePackages = {
        "ph.com.bdo.demo.api.classtype.repository.read",
        "ph.com.bdo.demo.api.student.repository.read",
        "ph.com.bdo.demo.api.studentclass.repository.read",
        "ph.com.bdo.demo.api.subject.repository.read",
        "ph.com.bdo.demo.api.sample.repository.read",
        "ph.com.bdo.demo.api.teacher.repository.read"},
    entityManagerFactoryRef = "readDatabaseEntityManager",
    transactionManagerRef = "readDatabaseTransactionManager"
)
@Configuration
public class DatabaseReadConfig {

    @Value("${spring.datasource.read.url}")
    private String url;

    @Value("${spring.datasource.read.username}")
    private String username;

    @Value("${spring.datasource.read.password}")
    private String password;

    @Bean(name = "readDataSource")
    public DataSource readDataSource() {
        return DataSourceBuilder
            .create()
                .url(url)
                .username(username)
                .password(password)
            .build();
    }

    @Bean(name = "readDatabaseEntityManager")
    public LocalContainerEntityManagerFactoryBean readDatabaseEntityManager(
            final EntityManagerFactoryBuilder builder,
            @Qualifier("readDataSource") final DataSource dataSource) {

        final Map<String, String> properties = new ConcurrentHashMap<>();

        properties.put(
            "hibernate.implicit_naming_strategy",
            "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy"
        );
        properties.put(
            "hibernate.physical_naming_strategy",
            "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy"
        );

        return builder
            .dataSource(dataSource)
            .packages("ph.com.bdo.demo.api")
            .persistenceUnit("model")
            .properties(properties)
            .build();
    }

    @Bean(name = "readDatabaseTransactionManager")
    public PlatformTransactionManager readTransactionManager(
            @Qualifier("readDatabaseEntityManager") final EntityManagerFactory entityManagerFactory) {

        return new JpaTransactionManager(entityManagerFactory);
    }
}
