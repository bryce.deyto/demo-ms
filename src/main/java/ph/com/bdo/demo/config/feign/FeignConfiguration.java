package ph.com.bdo.demo.config.feign;

import feign.Client;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContexts;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

@Configuration
public class FeignConfiguration {
    @Bean
    public Client feignClient() {
        return new Client.Default(getSslSocketFactory(), new NoopHostnameVerifier());
    }

    private SSLSocketFactory getSslSocketFactory() {
        try {
            final SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustSelfSignedStrategy()).build();
            return sslContext.getSocketFactory();
        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
