package ph.com.bdo.demo.config.feign;

import feign.Feign;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 */
@Configuration
public class FeignWithoutFallbackConfig {

    @Bean
    public Feign.Builder feignBuilder() {
        return Feign.builder().logLevel(Logger.Level.FULL);
    }

}
