package ph.com.bdo.demo.config.properties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SettingProperties.class)
public class PropertiesConfiguration {
}
