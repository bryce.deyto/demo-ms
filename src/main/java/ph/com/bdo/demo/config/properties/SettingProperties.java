package ph.com.bdo.demo.config.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Hashtable;

/**
 * Created By: john.santiago@novare.com.hk
 * Date created: 03/04/2021
 * For getting the value in application.yml
 */
@ConfigurationProperties(prefix = "settings")
@Data
@EqualsAndHashCode(callSuper = false)
public class SettingProperties extends Hashtable<String, SettingProperties.SettingConfig> {

    public SettingProperties() {
    }

    @Data
    public static class SettingConfig {

        private Integer limit;
        private Integer offset;
    }
}
