#!/bin/bash

echo "Deleting resources"
kubectl delete deployment.apps/transaction-mgmt-ms
kubectl delete service/transaction-mgmt-ms
kubectl delete configmap/transaction-mgmt-props
